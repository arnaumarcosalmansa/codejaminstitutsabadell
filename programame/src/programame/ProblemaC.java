package programame;

import java.util.Scanner;

public class ProblemaC {

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int times = input.nextInt();
		for(int i = 0; i < times; i++)
		{
			int nvagones = input.nextInt();
			int[][] vagones = new int[2][nvagones];
			
			int sumderecha = 0;
			int sumizquierda = 0;
			
			int sumprimero = 0;
			int sumfinal = 0;
			boolean par = nvagones % 2 == 0;
			
			for(int j = 0; j < nvagones; j++)
			{
				vagones[0][j] = input.nextInt();
				vagones[1][j] = input.nextInt();
				
				sumderecha += vagones[0][j];
				sumizquierda += vagones[1][j];
				
				if(j < nvagones / 2)
				{
					sumprimero += vagones[0][j] + vagones[1][j];
				}
				else if(par && j >= nvagones / 2)
				{
					sumfinal += vagones[0][j] + vagones[1][j];
				}
				else if(!par && j > nvagones / 2)
				{
					sumfinal += vagones[0][j] + vagones[1][j];
				}
			}
			/*
			System.out.println("derecha " + sumderecha);
			System.out.println("izquierda " + sumizquierda);
			System.out.println("principio " + sumprimero);
			System.out.println("final " + sumfinal);
			*/
			int vel = 300;
			vel -= Math.abs(sumderecha - sumizquierda) / 50 * 2;
			vel -= Math.abs(sumprimero - sumfinal) / 100 * 5;
			
			System.out.println(vel);
		}
	}

}

package programame;

import java.util.ArrayList;
import java.util.Scanner;

public class ProblemaA {

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int times = input.nextInt();
		int velocidad = input.nextInt();
		for(int i = 0; i < times; i++)
		{
			int siguienteVelocidad = 0;
			ArrayList<Integer> tramos = new ArrayList<Integer>();
			while(input.hasNextInt())
			{
				int tramo = input.nextInt();
				if(tramo > 3)
				{
					siguienteVelocidad = tramo;
					break;
				}
				else
				{
					tramos.add(tramo);
				}
			}
			
			int promedio = 0;
			for(Integer mul : tramos)
			{
				switch(mul)
				{
					case 1:
						promedio += velocidad;
						break;
					case 2:
						promedio += velocidad / 2;
						break;
					case 3:
						promedio += velocidad / 4;
						break;
				}
			}
			promedio /= tramos.size();
			System.out.println(promedio);
			velocidad = siguienteVelocidad;
		}
	}
}

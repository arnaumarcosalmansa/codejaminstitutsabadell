package programame;

import java.util.Scanner;

public class ProblemaE {

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int times = input.nextInt();
		input.nextLine();
		for(int j = 0; j < times; j++)
		{
			String frase = input.nextLine();
			int a = 0;
			int e = 0;
			int i = 0;
			int o = 0;
			int u = 0;
			
			for(int k = 0; k < frase.length(); k++)
			{
				switch(frase.charAt(k))
				{
					case 'a':
						a++;
						break;
					case 'e':
						e++;
						break;
					case 'i':
						i++;
						break;
					case 'o':
						o++;
						break;
					case 'u':
						u++;
						break;
				}
			}
			
			char vocal = ' ';
			if(a > e && a > i && a > o && a > u)
			{
				vocal = 'a';
			}
			else if(e > a && e > i && e > o && e > u)
			{
				vocal = 'e';
			}
			else if(i > a && i > e && i > o && i > u)
			{
				vocal = 'i';
			}
			else if(o > a && o > i && o > e && o > u)
			{
				vocal = 'o';
			}
			else if(u > a && u > i && u > o && u > e)
			{
				vocal = 'u';
			}
			System.out.println("Vocal guanyadora: " + vocal);
		}
	}

}

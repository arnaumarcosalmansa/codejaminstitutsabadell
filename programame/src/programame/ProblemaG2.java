package programame;

import java.util.ArrayList;
import java.util.Scanner;

public class ProblemaG2 {

	public static void main(String[] args)
	{
		Scanner input = new  Scanner(System.in);
		int times = input.nextInt();
		int primerNumero = 0;
		for(int i = 0; i < times; i++)
		{
			int ladoPiscina = input.nextInt();
			int nnadadores = input.nextInt();
			
			int[][] piscina = new int[ladoPiscina][ladoPiscina];
			ArrayList<Punto> puntos = new ArrayList<Punto>();
			Punto2 primerPunto = new Punto2();
			for(int j = 0; j < ladoPiscina; j++)
			{
				for(int k = 0; k < ladoPiscina; k++)
				{
					piscina[j][k] = input.nextInt();
					if(primerNumero == 0 && piscina[j][k] != 0)
					{
						primerNumero = piscina[j][k];
						primerPunto.x = j;
						primerPunto.y = k;
					}
				}
			}
			
			int[][] piscina2 = new int[ladoPiscina][ladoPiscina];
			Punto2 segundoPunto = new Punto2();
			for(int j = 0; j < ladoPiscina; j++)
			{
				for(int k = 0; k < ladoPiscina; k++)
				{
					piscina[j][k] = input.nextInt();
					if(piscina[j][k] == primerNumero)
					{
						primerNumero = piscina[j][k];
						segundoPunto.x = j;
						segundoPunto.y = k;
					}
				}
			}
			primerPunto.restar(segundoPunto);
			int distancia = primerPunto.calcDistancia();
		}
	}
}

class Punto2
{
	int x = 0;
	int y = 0;
	
	public void restar(Punto2 p)
	{
		this.x -= p.x;
		this.y -= p.y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Punto other = (Punto) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	public int calcDistancia()
	{
		return Math.max(Math.abs(this.x), Math.abs(this.y));
	}
}

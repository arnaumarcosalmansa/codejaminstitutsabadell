package programame;

import java.util.ArrayList;
import java.util.Scanner;

public class ProblemaH {

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int times = input.nextInt();
		input.nextLine();
		for(int i = 0; i < times; i++)
		{
			String snumero = input.nextLine();
			String[] splitted = snumero.split("");
			Integer[] digits = parseNums(splitted);
			
			boolean found = false;
			
			for(int j = 0; j < 10; j++)
			{
				int sum = 0;
				for(Integer in : digits)
				{
					sum += (in * in);
				}
				if(sum == 1)
				{
					found = true;
					break;
				}
				
				digits = getDigits(sum);
			}
			
			if(found)
			{
				System.out.println("HAPPY");
			}
			else
			{
				System.out.println("NO HAPPY");
			}
		}
	}
	
	public static Integer[] parseNums(String[] nums)
	{
		Integer[] digits = new Integer[nums.length];
		for(int i = 0; i < nums.length; i++)
		{
			digits[i] = Integer.parseInt(nums[i]);
		}
		return digits;
	}
	
	public static Integer[] getDigits(int num)
	{
		ArrayList<Integer> digits = new ArrayList<Integer>();
		while(num > 9)
		{
			digits.add(num % 10);
			num = num / 10;
		}
		digits.add(num);
		Integer[] result = new Integer[digits.size()];
		digits.toArray(result);
		return result;
	}
}

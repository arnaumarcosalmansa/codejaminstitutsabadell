package programame;

import java.util.Scanner;

public class ProblemaB {

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int times = input.nextInt();
		for(int i = 0; i < times; i++)
		{
			int x = input.nextInt();
			int y = input.nextInt();
			int z = input.nextInt();
			
			int albareda = (x + y - z) / 2;
			int torrubiano = (x - y + z) / 2;
			int vilelles = (-x + y + z) / 2;
			
			if(albareda < 0 || torrubiano < 0 || vilelles < 0)
			{
				System.out.println("IMPOSSIBLE");
			}
			else
			{
				System.out.println(albareda + " " + torrubiano + " " + vilelles);
			}
		}
	}

}

package programame;

import java.util.ArrayList;
import java.util.Scanner;

public class ProblemaD {

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int times = input.nextInt();
		for(int i = 0; i < times; i++)
		{
			int ncolumnas = input.nextInt();
			ArrayList<Integer> cols = new ArrayList<Integer>();
			for(int j = 0; j < ncolumnas; j++)
			{
				int num = input.nextInt();
				if(num != 0)
				{
					cols.add(num);
				}
			}
			
			Integer[] columnas = new Integer[cols.size()];
			cols.toArray(columnas);
			input.nextLine();
			
			String direccion = input.nextLine();
			
			int index = 0;
			boolean derecha = false;
			
			if(direccion.equals("R"))
			{
				index = columnas.length - 1;
				derecha = true;
			}
			
			ArrayList<Integer> result = new ArrayList<Integer>();
			
			if(derecha)
			{
				for(int j = index; j >= 0; j--)
				{
					if(j != 0 && columnas[j] == columnas[j - 1])
					{
						result.add(0, columnas[j] + columnas[j - 1]);
						j--;
					}
					else
					{
						result.add(0, columnas[j]);
					}
				}
				
				while(result.size() < ncolumnas)
				{
					result.add(0, 0);
				}
			}
			else
			{
				for(int j = index; j < columnas.length; j++)
				{
					if(j != columnas.length - 1 && columnas[j] == columnas[j + 1])
					{
						result.add(columnas[j] + columnas[j + 1]);
						j++;
					}
					else
					{
						result.add(columnas[j]);
					}
				}
				
				while(result.size() < ncolumnas)
				{
					result.add(0);
				}
			}
			
			for(int j = 0; j < result.size(); j++)
			{
				System.out.print(result.get(j));
				if(j < result.size() - 1)
				{
					System.out.print(' ');
				}
			}
			System.out.println();
		}
	}

}

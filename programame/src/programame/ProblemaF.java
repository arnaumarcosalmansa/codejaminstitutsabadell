package programame;

import java.util.Scanner;

public class ProblemaF {

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int times = input.nextInt();
		
		for(int i = 0; i < times; i++)
		{
			int t = input.nextInt();
			int[] terrain = new int[t];
			for(int j = 0; j < t; j++)
			{
				terrain[j] = input.nextInt();
			}
			
			int index = 0;
			travel(terrain, terrain[0] + 1, 0);
		}
	}
	
	
	public static void travel(int[] terrain, int height, int index)
	{
		if(index == terrain.length - 1) return;
		
		int diff = Math.abs(terrain[index + 1] - height);
		if(diff > 1) return;
		
		travel(terrain, height + 1, index + 1);
		travel(terrain, height, index + 1);
		travel(terrain, height - 1, index + 1);
	}
}

package programame;

import java.util.HashSet;
import java.util.Scanner;

public class ProblemaG {

	public static void main(String[] args)
	{
		Scanner input = new Scanner(System.in);
		int times = input.nextInt();
		for(int i = 0; i < times; i++)
		{
			int ladoPiscina = input.nextInt();
			int nnadadores = input.nextInt();
			
			int[][] piscina1 = new int[ladoPiscina][ladoPiscina];
			Punto[] nadadores = new Punto[5];
			for(int j = 0; j < ladoPiscina; j++)
			{
				for(int k = 0; k < ladoPiscina; k++)
				{
					piscina1[j][k] = input.nextInt();
					if(piscina1[j][k] != 0)
					{
						Punto p = new Punto();
						p.x = j;
						p.y = k;
						nadadores[piscina1[j][k] - 1] = p;
					}
				}
			}
			
			int[][] piscina2 = new int[ladoPiscina][ladoPiscina];
			Punto[] nadadoresDespues = new Punto[5];

			for(int j = 0; j < ladoPiscina; j++)
			{
				for(int k = 0; k < ladoPiscina; k++)
				{
					piscina2[j][k] = input.nextInt();
					if(piscina2[j][k] != 0)
					{
						Punto p = new Punto();
						p.x = j;
						p.y = k;
						nadadoresDespues[piscina2[j][k] - 1] = p;
					}
				}
			}
			HashSet<Punto> movs = new HashSet<Punto>();
			for(int j = 0; j < nnadadores; j++)
			{
				if(nadadoresDespues[j] != null)
				{
					nadadoresDespues[j].restar(nadadores[j]);
					movs.add(nadadoresDespues[j]);
				}
			}
			
			if(movs.size() != 1)
			{
				System.out.println("NO SINCRONITZADA");
			}
			else
			{
				int d = nadadoresDespues[0].calcDistancia();
				System.out.println(d);
			}
		}
	}
}

class Punto
{
	int x = 0;
	int y = 0;
	
	public void restar(Punto p)
	{
		this.x -= p.x;
		this.y -= p.y;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + x;
		result = prime * result + y;
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		Punto other = (Punto) obj;
		if (x != other.x)
			return false;
		if (y != other.y)
			return false;
		return true;
	}
	
	public int calcDistancia()
	{
		return Math.max(Math.abs(this.x), Math.abs(this.y));
	}
}
